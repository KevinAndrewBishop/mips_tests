###Lottery odds calculator.
#calculate odds of winning a lottery. Lotteries work by using two pools of
#numbers. You must match a certain subset of each pool to win the lottery.
#e.g. you must get 5 of 50 (first pool) and 2 of 11 (second pool).
#The formula is then (50 choose 5)x(11 choose 2)
	.globl	main
main:
	.text
	#read N and K for each pool, calculate their combination and product
	#print string telling user to enter numbers for the large pool
	li $v0, 4				# system call code for print_str
	la $a0, str_large		# address of string to print
	syscall 				# print the string
	jal read				#read N and K from user for 1st pool
	add $a0, $zero, $v0 	#set N as argument a0
	add $a1, $zero, $v1		#set K as argument a1
	jal combo				#calculate N choose K
	addi $s0, $v0, 0		#save value in s0
	#print string telling user to enter numbers for the small pool
	li $v0, 4				# system call code for print_str
	la $a0, str_small		# address of string to print
	syscall 				# print the string
	jal read				#read N and K from user for 2nd pool
	add $a0, $zero, $v0 	#set N as argument a0
	add $a1, $zero, $v1		#set K as argument a1
	jal combo				#calculate N choose K
	mul $s0, $s0, $v0		#multiply first combo and second

	#Print the results and exit
	addi $v0, $zero, 4	# print string
	la $a0, str		# the text for output
	syscall 		# call opsys
	addi $v0, $zero, 1	# print integer
	add $a0, $zero, $s0	# the integer is sum
	syscall			# call opsys
	addi $v0, $zero, 4	# print string
	la $a0, stopped		# the text for output
	syscall 		# call opsys
	addi $v0, $zero, 10	# finished .. stop .. return
	syscall 		# to the Operating System

####Read lottery numbers subroutine
#reads the numbers N and K from the user and stores them in v0 and v1
read:
	li $v0, 4	# system call code for print_str
	la $a0, str_n	# address of string to print
	syscall 	# print the string
	li $v0, 5 	# system call code for read_int
	syscall 	# print it
	addi $t0, $v0, 0	#store large N in t0
	li $v0, 4	# system call code for print_str
	la $a0, str_k	# address of string to print
	syscall 	# print the string
	li $v0, 5 	# system call code for read_int
	syscall 	# print it
	addi $v1, $v0, 0	#store k in v1
	addi $v0, $t0, 0	#store N in v0
	jr $ra			#return N and K

#combination subroutine
#given N and K in registers $a0 and $a1, calculate N choose K
#the result is stored in register $v0
combo:
	addi $v0, $zero, 1	#initialize output to 1
	addi $t1, $a1, 1 	#counter t1 = K + 1
loop:
	slt $t2, $t1, $a0	#t2 = 1 if counter < N
	beq $t2, $zero, done#break out of loop
	mul $v0, $v0, $t1	#$v0 = $v0*i
	sub $t0, $t1, $a1	#calculate i-k
	div $v0, $v0, $t0	#$v0 = $v0/(i-k)
	addi $t1, $t1, 1	#increment counter
	jr loop
done:
	mul $v0, $v0, $t1	#$v0 = $v0*i
	sub $t0, $t1, $a1
	div $v0, $v0, $t0	#$v0 = $v0/(i-k)
	jr $ra				#return the value of N choose K

	.data
str: .asciiz "\nThe odds of winning this lottery is 1 in "
str_n: .asciiz "Enter the pool of possible numbers: "
str_k: .asciiz "Enter the count of numbers to select from the pool: "
str_large: .asciiz "Enter N and K for the first, larger pool:\n"
str_small: .asciiz "Enter N and K for the second, smaller pool:\n"
stopped:
	.asciiz "\nStopped."
