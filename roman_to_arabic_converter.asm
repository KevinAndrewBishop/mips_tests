###ROMAN NUMERAL TO ARABIC NUMERAL CONVERTER
##Accepts a roman numeral as input from standard in
##Converts it to arabic decimal numerals and prints to stdout
##Executed using QTSPIM MIPS simulator
##Written in MIPS assembly language
.globl	main
main:				   #convert numerals
    .text
    jal read            #read user input
    addi $a0, $v0, 0    #copy string address to a0
    jal convert         #convert to arabic
    beq $v0, $zero, exit#if user entered nothing, exit
    addi $a0, $v0, 0    #copy arabic number to a0
    li $v0, 1	        #system call code for print_int
    syscall             #print the digit
    jr main             #loop back and ask for next digit
exit: #exit program if user enters nothing
    addi $v0, $zero, 4	# print string
    la $a0, stopped		# the text for output
    syscall 		    # call opsys

    addi $v0, $zero, 10	# finished .. stop .. return
    syscall 		     # to the Operating System


####Read roman numerals routine
#reads the numeral from the user and stores string in v0
read:
    li $v0, 4	         # system call code for print_str
    la $a0, str	         # address of string to print
    syscall 	         # print the string

    li $v0, 8	         # system call code for read_str
    la $a0, buffer       # address to store string
    addi $a1, $zero, 20  # max length of string
    syscall 	          # read the string
    addi $v0, $a0, 0      #save string in v0
    jr $ra			      #return string

###Subroutine to convert individual roman letter into arabic number
#e.g. I = 1, V= 5, X = 10 etc.
#returns -1 if character not a valid roman numeral (IVXLCDM)
#letter (char) should be in $a0, and digit will be in $v0
get_digit:
    addi $t0, $zero, -72   #set t0 to difference between decimal and ascii value
    addi $v0, $zero, 73    #ASCII I = 73
    beq $v0, $a0, done
    addi $t0, $zero, -81
    addi $v0, $zero, 86    #ASCII V = 86
    beq $v0, $a0, done
    addi $t0, $zero, -78
    addi $v0, $zero, 88    #Roman numeral X = 88
    beq $v0, $a0, done
    addi $t0, $zero, -26
    addi $v0, $zero, 76    #Roman numeral L = 76
    beq $v0, $a0, done
    addi $t0, $zero, 33
    addi $v0, $zero, 67    #Roman numeral C = 67
    beq $v0, $a0, done
    addi $t0, $zero, 432
    addi $v0, $zero, 68    #Roman numeral D = 68
    beq $v0, $a0, done
    addi $t0, $zero, 923
    addi $v0, $zero, 77    #Roman numeral M = 77
    beq $v0, $a0, done
    addi $t0, $zero, -1     #If no match yet, return -1
    addi $v0, $zero, 0
done:
    add $v0, $v0, $t0       #add t0 to get decimal
    jr $ra                 #return decimal

####Roman numeral conversion Subroutine
#expects string location in $a0
#returns arabic number in $v0
convert:
    addi $sp, $sp, -12      #adjust stack for three more items
    sw $s0, 0($sp)          #save $s0
    sw $s1, 4($sp)          #save s1
    sw $ra, 8($sp)          #save register
    addi $s0, $zero, 0      #initialize decimal to 0
    addi $s1, $a0, 0        #s1 = string pointer
    jr conv_loop
conv_loop:                  #loop through numerals and add to total
    lbu $a0, 0($s1)         #load first character into a0
    jal get_digit           #get the next digit
    addi $t0, $zero, -1     #set t0 to -1 to test if null
    beq $t0, $v0, conv_done #exit loop if digit is null
    addi $t1, $v0, 0        #save digit in t1
    addi $s1, $s1, 1        #get address of next roman numeral
    lbu $a0, 0($s1)         #load second character into a0
    jal get_digit           #convert next digit
    addi $t0, $zero, -1     #set t0 to -1 to test if null
    beq $t0, $v0, conv_single#if second digit is null, add single
    addi $t2, $v0, 0        #otherwise save second digit in t2
    slt $t0, $t1, $t2       #t0 = 1 if first digit less than second
    bne $t0, $zero, conv_subtract#if first digit is less than second
    add $s0, $s0, $t1       #else add the first number to the total
    jr conv_loop            #loop back to next numeral
conv_subtract:              #handle cases like IX where we subtract X - I
    sub $t0, $t2, $t1       #calculate difference second - first
    add $s0, $s0, $t0       #add the difference to the total
    addi $s1, $s1, 1        #get address of next roman numeral
    jr conv_loop            #loop back to next numeral
conv_single:                #handle cases of singletons like I or X
    add $s0, $s0, $t1       #add digit to total
    jr conv_done
conv_done:                  #after loop is done, pop items back of stack and return value
    add $v0, $zero, $s0     #set v0 to total
    lw $s0, 0($sp)          #load previous s0
    lw $s1, 4($sp)          #load previous s1
    lw $ra, 8($sp)          #load previous ra
    addi $sp, $sp, 12       #pop 2 items off stack
    jr $ra                  #return numeral


.data
    buffer: .space 20
    str: .asciiz "\nEnter a Roman Numeral (or nothing to exit): "
    stopped: .asciiz "\nStopped."
